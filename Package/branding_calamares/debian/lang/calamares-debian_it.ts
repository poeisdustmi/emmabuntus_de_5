<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>show</name>
    <message>
        <location filename="../show.qml" line="45"/>
        <source>Welcome to Debian 12 GNU/Linux - Emmabuntus DE 5.&lt;br/&gt;The rest of the installation is automated and should complete in a few minutes.</source>
        <translation>Benvenuti in Debian 12 GNU/Linux - Emmabuntüs DE 5.&lt;br/&gt;Il resto dell'installazione è automatizzato e dovrebbe essere completato in pochi minuti.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="67"/>
        <source>More customization available through the post-installation process.</source>
        <translation>Maggiore personalizzazione disponibile attraverso il processo post-installazione.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="88"/>
        <source>More accessibility for everyone, thanks to the three dock levels !</source>
        <translation>Più accessibilità per tutti, grazie ai tre livelli del dock!</translation>
    </message>
    <message>
        <location filename="../show.qml" line="109"/>
        <source>More than 60 software programs available, which allow the beginners to discover GNU/Linux.</source>
        <translation>Più di 60 programmi software disponibili, che consentono ai principianti di scoprire GNU/Linux.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="130"/>
        <source>More than 10 edutainment software programs for training, including Kiwix the offline Wikipedia reader.</source>
        <translation>Più di 10 programmi software di edutainment per la formazione, tra cui Kiwix, il lettore di Wikipedia offline.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="151"/>
        <source>Emmabuntus supports social and environmental projects through the Lilo ethical and solidarity research engine.</source>
        <translation>Emmabuntüs sostiene progetti sociali e ambientali attraverso il motore di ricerca etico e solidale di Lilo.</translation>
    </message>
        <message>
        <location filename="../show.qml" line="171"/>
        <source>Emmabuntus collaborates with more than 10 associations, particularly in Africa, in order to reduce the digital divide.</source>
        <translation>Emmabuntüs collabora con più di 10 associazioni, in particolare in Africa, al fine di ridurre il divario digitale.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="191"/>
        <source>Emmabuntus together with YovoTogo and JUMP Lab'Orione have equipped and run 32 computer rooms in Togo.</source>
        <translation>Emmabuntüs insieme a YovoTogo e JUMP Lab'Orione hanno attrezzato e gestito 32 sale computer in Togo.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="211"/>
        <source>Emmabuntus is also a refurbishing key, based on Ventoy,&lt;br/&gt;allowing you to quickly and easily refurbish a computer with GNU/Linux.</source>
        <translation>Emmabuntüs è anche una chiave di ristrutturazione, basata su Ventoy,&lt;br/&gt;che ti consente di rinnovare rapidamente e facilmente un computer con GNU/Linux.</translation>
    </message>
</context>
</TS>
